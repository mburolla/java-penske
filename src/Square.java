
final public class Square extends Shape {

	//
	// Constructor
	//
	
	public Square(String color) {
		super(color); // Call our Shape constructor.
	}

	//
	// Override
	//
	
	@Override
	public String draw() {
		return "Draw " + color + " Square...";  // color is defined in the base class (Shape).
	}
}
