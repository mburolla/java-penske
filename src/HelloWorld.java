// import java.util.Arrays;  // Cannot be resolved???
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

import com.xpanxion.data.Dog;
import com.xpanxion.data.Person;
import com.xpanxion.tab.TabSnippetManager;

public class HelloWorld {

	public static void main(String[] args) {
		exceptions();
	}
	
	private static void exceptions() {
		try {
			doWork();
		}
		catch(DataAccessException dae) {
			dae.printStackTrace();
		}
		catch(ArithmeticException ae) {
			ae.printStackTrace();
			// We would want to handle this error as best as we can.
			// And if we can't we wouldn't re-throw it. ==> throw ae;
		}
		catch(Exception e) {
			e.printStackTrace();
		}
//		catch(Exception e) { // Never do this.
			// Swallowing exceptions hides errors and
			// gives false sense of security.
//		}
//		catch(DataAccessException dae) { // Will not compile.
//			System.out.println(dae);
//		}
		finally {
			System.out.println("Finally blocks always get executed.");
		}
	}
	
	private static void doWork() throws Exception {
		var result = 7/0; // throws divide by zero error.
		//throw new Exception("Fatal Error doWork() is broken.");
		//throw new DataAccessException("Database is down!");
		// System.out.println("Work is finished.");
	}
	
	private static void writeFile1() {
		try {
            var file = new File("myfile.txt");
            if (file.createNewFile()) {
                System.out.println("File created: " + file.getName());
            }
            else {
                System.out.println("File already exists.");
            }
            System.out.println(file.getAbsolutePath()); // <== Kind of nice.
        }
        catch (IOException e) { // <== Exceptions makes our programs fail gracefully.
            System.out.println("An error has occurred.");
            e.printStackTrace();
        }
	}
	
	private static void writeFile2() {
        try {
            var writer = new FileWriter("myfile.txt");
            writer.write("Files in Java are seriously good!!");
            writer.close();
            System.out.println("Successfully written.");
        }
        catch (IOException e) {
            System.out.println("An error has occurred.");
            e.printStackTrace();
        }
	}
	
	private static void writeFileUsingTryWithResources() {
		// FileWriter implements autoclosable, so we can use the Try-With-Resources pattern.
        try (var writer = new FileWriter("myfile.txt")) {
        	writer.write( "Try with resources is slick.");
            System.out.println("Successfully written.");
        }
        catch (IOException e) {
            System.out.println("An error has occurred.");
            e.printStackTrace();
        }
	}
	
	private static void readFileUsingTryWithResources() {
        var file = new File("myfile.txt");
        // Scanner implements autoclosable, , so we can use the Try-With-Resources pattern.
        try (var reader = new Scanner(file)) {
            while (reader.hasNextLine()) {
                String data = reader.nextLine();
                System.out.println(data);
            }
        }
        catch (FileNotFoundException e) {
            System.out.println("An error has occurred.");
            e.printStackTrace();
        }
	}
	
	private static void oopConcepts() {
		var p1 = new Person("Mary", "Smith", 22);
		var message = p1.speak();
		System.out.println(message);
	}
	
	private static void copying() {
		// Note: Strings in Java are an immutable reference types.
	
		// Shallow copy.
		var p1 = new Person("Mary", "Smith", 22);
		var p2 = p1;
		
		p1.setAge(23);
		p1.setFirstName("MARY");
		System.out.println(p1);
		System.out.println(p2);
		
		// Deep copy with Cloneable.
//		var p3 = new Person("Mary", "Smith", 22);
//		var p4 = p3.clone();
//		
//		p3.setAge(23);
//		p3.setFirstName("MARY");
//		System.out.println(p3);
//		System.out.println(p4);
		
		// Deep copy with copy constructor.
//		var p5 = new Person("Mary", "Smith", 22);
//		var p6 = new Person(p5); // Calls the copy constructor.
//		p5.setAge(23);
//		p5.setFirstName("MARY");
//		System.out.println(p5);
//		System.out.println(p6);

	}
	
	private static void methods() {
		// Declarations...
		int number = 22; // Primitive type
		var mary = new Person("Mary", "Jones", 22);  // Reference type
		var stringArray = new String[] {"a", "b", "c"};
		var methodHelper = new MethodHelper();
		
		// Primitive types are passed by value (AKA copy)
		// Reference types are passed by reference
//		methodHelper.update(mary, number); 
//		System.out.println(mary.getAge());
//		System.out.println(number);
		
//		var alice = methodHelper.createPerson("Alice", "Jones", 23);		
//		System.out.println(alice);
		
//		methodHelper.update(stringArray);
//		System.out.println(stringArray[0]);		
//		System.out.println(stringArray[1]);		
//		System.out.println(stringArray[2]);		
	}
	
	private static void arrays() {
		// Arrays start at zero (they are zero based indexed)
		// Once an array has been declared, it cannot be resized.
		var stringArray = new String[] {"a", "b", "c"};  // One dimensional array of Strings
		var numArray = new int[] {1,2,3,4,5};
		var numArray2 = new int[10];
		
		// Types of array errors:
		// numArray[0] = "test"; // Compile time: Type mismatch
		// stringArray[0] = 1; // Compile time: Type mismatch 
		// System.out.println(numArray[5]); // Run time: Index 5 out of bounds for length 5
		// numArray2[10] = 5150; // Run time: Index 10 out of bounds for length 10
		
		var stringArray2d = new String[][] {{"a", "aa"}, {"b", "bb"}, {"c", "cc"}};  // Two dimensional array
		// Indexing example...
		System.out.println(stringArray2d[0][0]); // a
		System.out.println(stringArray2d[0][1]); // aa
		System.out.println(stringArray2d[1][0]); // b
		System.out.println(stringArray2d[1][1]); // bb
		System.out.println(stringArray2d[2][0]); // c
		System.out.println(stringArray2d[2][1]); // cc
	}
	
	private static void polymorphism() {
		var shapeList = new ArrayList<Shape>(); // Shape is the base class.
		
		shapeList.add(new Circle("red"));
		shapeList.add(new Square("yellow"));
		
		// Iterative approach...
//		for (int i = 0; i < shapeList.size(); i++) {
//			System.out.println(shapeList.get(i).draw()); // The draw method is bound at runtime.
//		}
		
		// Functional approach...
		shapeList.forEach(s -> System.out.println(s.draw())); // The draw method is bound at runtime.
		                 // -> means this is a lambda function
		                 // s is my abbreviation for shape
	}
	
	private static void misc() {
		String[] catList = {"Gypsy", "Rocky", "Baxie", "Lily", "Gabby"};

		Person p1 = new Person();
		
		Person p2 = new Person("Joe", "Green", 33); // Concrete class.
		Person p3 = new Person("Mary", "Smith", 22, catList);
	
		String message = p2.speak();
		System.out.println(message);
		
		var result = p3.numberOfYs();
		System.out.println(result);
		System.out.println(Person.personCount); // Static.
		
		var c = new Calculator(); // Type Inference.
		var r = c.add(1, 2);
		r = c.add(3, 4);
	}
}
