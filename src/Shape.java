
public abstract class Shape { // abstract means this cannot be instantiated.

	//
	// Data members
	//
	private int a = 0; // This will not be available in the subclass.
	protected String color; // protected => this is available in this class and in derived classes.
	protected abstract String draw(); // protected abstract => this MUST be implemented in derived classes.
	
	//
	// Constructor
	//
	
	public Shape(String color) {
		this.color = color;
	}
}
