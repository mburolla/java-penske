
public class DataAccessException extends Exception { // Inheritance.
	
	//
	// Constructors
	//
	
	public DataAccessException() {
		super();
	}
	
	public DataAccessException(String message) {
		super(message); // This calls the constructor for the Exception class.
	}
}
