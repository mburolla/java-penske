
final public class Circle extends Shape {

	//
	// Constructor
	//
	
	public Circle(String color) {
		super(color); // Call our Shape constructor.
	}

	//
	// Overrides 
	//
	
	@Override
	public String draw() {
		return "Draw " + color + " Circle..."; // color is defined in the base class (Shape).
	}
}
