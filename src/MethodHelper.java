import com.xpanxion.data.Person;

public class MethodHelper {

	//
	// Constructors
	//
	
	public MethodHelper() { }// No arg constructor.
	
	//
	// Public
	//
	
	public void update(Person person) { // Overloading the update method.
		person.setAge(5150);
	}
	
	public void update(Person person, int a) { // Overloading the update method.
		a = 5150;
		person.setAge(5150);
	}
	
	public void update(String[] stringArray) { // Overloading the update method.
		// This is a reference type, so we can update it in our method.
		stringArray[2] = "Top Jimmy"; // Arrays are zero based.
	}
	
	public Person createPerson(String firstName, String lastName, int age) {  // Factory pattern.
		return new Person(firstName, lastName, age);
	}
}
