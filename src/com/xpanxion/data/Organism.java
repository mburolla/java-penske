package com.xpanxion.data;

public abstract class Organism {

	//
	// Data members
	//
	
	final protected int test = 0;
	protected int age;
	
	//
	// Constructors
	//
	
	public Organism() {
		
	}
	
	public Organism(int age) {
		this.age = age;
	}
}
