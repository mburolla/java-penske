package com.xpanxion.data;

// "Business contracts" in our code that tell us what something can do, and
//  they can also hold data.
public interface Speakable {
	
	int volume = 10; // This is final by default.
	
	public String speak(); // <== think about this use case first.
	
	// Hold my beer...
	// Java 8 broke the mold, interfaces can have implementation logic!
	// Save this type of stuff for extending legacy code.
	default Boolean isLoud() {  // <== don't think about this for new code.
		return true;
	}
}
