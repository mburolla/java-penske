package com.xpanxion.data;

public class Dog {

	private String firstName;
	private String lastName;
	private Integer age;
	
	
	public Dog(String firstName, String lastName, Integer age) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
	}
	
	public String Speak() {
		var sb = new StringBuilder();
		sb.append("Woof my name is ");
		sb.append(firstName);
		sb.append(" ");
		sb.append(lastName);
		sb.append(" and I am ");
		sb.append(age);
		sb.append(" years old.");
		return sb.toString();
	}
	
	
}
