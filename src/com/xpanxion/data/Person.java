package com.xpanxion.data;

// Reads: Person "is an" Organism that "can be" speakable and cloneable.
// Java support single class inheritance, but can implement many interfaces.
public class Person extends Organism
	implements Speakable, Cloneable { 
	
	//
	// Data members
	//
	
	public static Integer personCount = 0; // Class scope.
	
	private String ssn; // private is one of the three access modifiers (private, protected, public).
	private String[] cats;
	private String lastName;
	private String firstName; // Instance variables.
	
	//
	// Constructors
	//
	
	public Person(Person person) {  // Copy constructor.
		this.age = Integer.valueOf(person.age); // Copy from our base class.
		this.firstName = new String(person.firstName); // Need to create a NEW String.
		this.lastName = new String(person.lastName);  // Need to create a NEW String
		// SSN cannot be copied because it's private.
		personCount++;
	}
	
	// Function signature: Empty
	public Person() {  // (No arg) Empty. Constructors do NOT have a return type.
		personCount++;
	}
	
	// Function signature: String
	public Person(String firstName) { // Constructor overloading (method overloading)
		this.firstName = firstName;
		personCount++;
	}
	
	// Function signature: String, String, Integer
	public Person(String firstName, String lastName, Integer age) { // Constructor overloading (method overloading)
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
		personCount++;
	}
	
	// Function signature: String, String, Integer, String[]
	public Person(String firstName, String lastName, Integer age, String[] cats) { // Constructor overloading (method overloading)
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
		this.cats = cats;
		personCount++;
	}
	
	//
	// Public
	//
	
	public void test() {
//		this.test = 11;  // This will not compile.
//		this.volume = 11;
	}
	
	public static String speak2() { // We do not need to instantiate the Person class.
		// We must call static methods.
		// speak();  // Compile time error, trying to call non-static method.
		return "Hello";
	}
	
	/**
	 * Returns the number of "y"s for all the cat names.
	 * @return
	 */
	public Integer numberOfYs() { 
		Integer retval = 0;
		// Nested loop...
		for (int i = 0; i < cats.length; i++) { // For each cat...
			var cat = cats[i]; // var => Type Inference.
			for (int k = 0; k < cat.length(); k++) { // For each letter in cat...
				var letter = cat.charAt(k);
				if (Character.toString(letter).equalsIgnoreCase("y")) { // toString() is static.
					retval++;
				}
			}
		}
		return retval;
	}
	
	//
	// Getters & Setters: Act as protectors/validators to our private data store.
	//
	
	public Integer getAge() {
		return age;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setAge(Integer age) {
		this.age = age;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	//
	// Private: Helper method used only internally to this class.
	//
	
	private String buildMessage() {
		// Use the StringBuilder for LARGE strings.
		// Strings are immutable in Java.
		var stringBuilder = new StringBuilder(); // var ==> Type Inference.
		stringBuilder.append("My name is ");
		stringBuilder.append(firstName);
		stringBuilder.append(" ");
		stringBuilder.append(lastName);
		stringBuilder.append(" and my age is: ");
		stringBuilder.append(age);
		stringBuilder.append(".");
		return stringBuilder.toString();
	}

	//
	// Overrides
	//
	
	@Override
	public String toString() {
		return "Person [age=" + age + ", lastName=" + lastName + ", firstName=" + firstName + "]";
	}
	
	@Override
	public String speak() {
		return "Hello, my name is " + firstName + " " + lastName + ".";
	}
	
    @Override
    public Object clone() {
    	var retval = new Person();
    	retval.firstName = new String(this.firstName);
    	retval.lastName = new String(this.lastName);
    	retval.age = Integer.valueOf(this.age); // Base class.
    	return retval;
    }
}
