package com.xpanxion.tab;

import java.util.ArrayList;
import java.util.List;

public class TabSection {

	//
	// Data members
	//
	
	private List<String> fretSpan;
	
	//
	// Constructors
	//
	
	public TabSection(Integer numStrings) {
		fretSpan = new ArrayList<String>();
		for (int i = 0; i < numStrings; i++) {
			fretSpan.add("-");
		}
	}
	
	//
	// Public
	//
	
	public String getNoteValueForStringNumber(Integer stringNumber) {
		String retval = "";
		retval = this.fretSpan.get(stringNumber);
		if (retval.length() == 1 && doesContainHighFretNumber()) {
			retval += "-";
		}
		return retval;
	}
	
	public void update(Integer stringNumber, Integer fretValue) {
		fretSpan.set(stringNumber, fretValue.toString());
	}
	
	//
	// Private
	//
	
	private boolean doesContainHighFretNumber() {
		boolean retval = false;
		for (String s : fretSpan) {
			if (!s.equals("-") && Integer.parseInt(s) > 10) {
				retval = true;
				break;
			}
		}
		return retval;
	}
	
}
