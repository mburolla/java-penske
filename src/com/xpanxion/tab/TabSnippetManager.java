package com.xpanxion.tab;

import java.util.ArrayList;

public class TabSnippetManager {

	//
	// Data Members
	//
	
	String[] tunning = {"E", "A", "D", "G", "B", "E"}; // 0 ==> Low E
	private ArrayList<TabSnippet> tabSnippetList;
	
	//
	// Constructors
	//
	
	public TabSnippetManager(String[] tunning) {
		this.tunning = tunning;
		tabSnippetList = new ArrayList<TabSnippet>();
		tabSnippetList.add(new TabSnippet(tunning));
	}
	
	//
	// Public
	//
	
	public void upsertTabSnippet(Integer tabSnippetIndex, Integer tabPositionIndex, Integer stringNumber, Integer fretNumber) {
		TabSnippet tabSnippet;
		if (tabSnippetIndex > tabSnippetList.size()) {
			tabSnippet = new TabSnippet(tunning);
			tabSnippetList.add(tabSnippet);
		} else {
			tabSnippet = tabSnippetList.get(tabSnippetIndex);
		}
		
		tabSnippet.updateTabSection(tabSnippetIndex, stringNumber, fretNumber);
		tabSnippet.createNewTabSection(tabSnippetIndex + 1);
	}
	


	public String getTabSnippetString(Integer tabSnippetIndex) {
		return tabSnippetList.get(tabSnippetIndex).toString();
	}
	
	
	//
	// Private
	//
	
}
