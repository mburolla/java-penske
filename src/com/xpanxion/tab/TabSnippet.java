package com.xpanxion.tab;

import java.util.ArrayList;

public class TabSnippet {

	//
	// Data members
	//
	
	private String[] tunning;
	private ArrayList<TabSection> tabSectionList;

	//
	// Constructors
	//
	
	public TabSnippet(String[] tunning) {
		super();
		this.tunning = tunning;
		tabSectionList = new ArrayList<TabSection>();
	}

	//
	// Methods
	//
	
	public void createNewTabSection(Integer tabSnippetIndex) {
		if (tabSnippetIndex >= tabSectionList.size()) {
			TabSection tabSection = new TabSection(tunning.length);
			tabSectionList.add(tabSection);
		} 
	}
	
	public void updateTabSection(Integer tabSnippetIndex, Integer stringNum, Integer fretNumber) {
		
		TabSection tabSection = null;
		if (tabSnippetIndex >= tabSectionList.size()) {
			tabSection = new TabSection(tunning.length);
			tabSectionList.add(tabSection);
		} else {
			tabSection = tabSectionList.get(tabSnippetIndex);
		}
		tabSection.update(stringNum, fretNumber);
	
	}
	
	//
	// Overrides
	//
	
	@Override
	public String toString() {
		var sb = new StringBuilder();

		for (Integer i=tunning.length - 1; i > -1; i--) {
			sb.append(tunning[i]);
			sb.append(" ");
			
			Integer stringNumber = i;
			tabSectionList.forEach(ts -> {
				var t =  ts.getNoteValueForStringNumber(stringNumber);
				sb.append(t);
			});
			sb.append("\r\n");
		}
	
		return sb.toString();
	}
	
}
