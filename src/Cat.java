
public class Cat {

	//
	// Data members
	//
	
	private String name;
	private Integer age;
	
	//
	// Constructors
	//
	
	public Cat(String name, Integer age) {
		this.name = name;
		this.age = age;
	}
	
	
	//
	// Public Methods
	//
	
	public String Speak() {
		return "My name is " + this.name;
	}
	
	//
	// Private
	//
	
	//
	// Overrides
	//
	
	@Override
	public String toString() {
		return "Cat [name=" + name + ", age=" + age + "]";
	}
	
	
	
}
