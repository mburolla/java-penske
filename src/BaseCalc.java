import java.util.ArrayList;
import java.util.List;

public abstract class BaseCalc { // Super class (AKA Parent class)

	//
	// Data members
	//
	
	protected List<String> historyList; // public, private, protected.
	
	//
	// Constructors
	//
	
	public BaseCalc() {
		historyList = new ArrayList<String>();
	}
}
