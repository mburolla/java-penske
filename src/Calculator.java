import java.util.ArrayList;
import java.util.List;

public class Calculator extends BaseCalc { // Calculator is the child class (AKA Subclass)
	
	//
	// Constructors
	//
	
	public Calculator() {
	
	}
	
	//
	// Public
	//
	
	public Integer add(Integer a, Integer b) {
		Integer retval = 0;
		retval = a + b;
		historyList.add("Add " + a + " + " + b + " = " + retval.toString());
		return retval;
	}
	
	public List<String> getHistory() {
		return historyList;
	}
	

}
