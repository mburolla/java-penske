# Java Penske
Helper project.

# Agenda

#### 4/13/2022
- Exceptions 
	- Guidelines
	  - Catch the most specific Exception first
	  - If you cannot appropriately handle the Exception in your catch block, re-throw it
	  - Never swallow Exceptions
	  - Do not use Exceptions for program flow (in place of if-else statements)
	- [Custom Exceptions](https://www.baeldung.com/java-new-custom-exception) 

#### 4/12/2022
- Java Overview
  - [Java Classes](https://docs.oracle.com/en/java/javase/18/docs/api/java.base/module-summary.html)
  - [Java Hierarchy](https://docs.oracle.com/en/java/javase/18/docs/api/overview-tree.html)
- File I/O
  - File object
  - Streams
  - Streams: [Try With Resources](https://www.baeldung.com/java-try-with-resources)

#### 4/11/2022
- OOP Concepts
  - Inheritance: "Is a..."
  - Composition: "Has a..."
  - Interface: "Is a...(Can be...)"
    - Loose coupling
    - Variables are final, public and static
- Copying objects
  - Shallow Copy vs Deep Copy
#### 4/7/2022
- Methods
- Arrays
- Review for Quiz tomorrow

#### 4/6/2022
- 4 Pillars OOP
  - Abstraction: Hiding implementation details
    - Abstractions are EVERYWHERE!
  - Encapsulation: Hiding data (private, protected)
  - Inheritance: "Is a" relationship
    - The subclass (or derived class) will contain anything that is NOT private in the super class
  - Polymorphism: Literal translation "many forms"
    - `@Override` annotation
    - [Nerd alert: Virtual functions](https://www.javatpoint.com/virtual-function-in-java)
- 5 Pillars OOP
  - Composition: "Has a"
  
- Methods
  
#### 4/5/2022
- Class private methods
- Loops
  - The debugger is your best friend!
    - Double click, create breakpoint
    - Press F11
    - Press F6 to step
    - Watch the variables tab
  - [Examples](https://gitlab.com/mburolla/java-intro/-/blob/main/src/main/java/com/xpanxion/java/intro/Worker.java#L160)
  - [Functional Programming](https://gitlab.com/mburolla/java-assignments-2/-/blob/dev/src/main/java/com/xpanxion/java/assignments/instructor/Worker0.java)
- Static
- Inheritance (protected)
- 4 Pillars

#### 4/4/2022
- Classes
  - Data & Behavior
    - Methods:
      - Constructor
	  - Getters & Setters
	 - Public (did not get to private)
- Method overloading
- Packages

# Java Repos
- [Intro](https://gitlab.com/mburolla/java-intro/-/blob/main/src/main/java/com/xpanxion/java/intro/Worker.java)
- [OOP](https://gitlab.com/mburolla/java-oop/-/tree/main/src/main/java/com/xpanxion/com/java/oop)
- [Data Structures](https://gitlab.com/mburolla/java-collections/-/blob/main/src/main/java/com/xpanxion/java/collections/Worker.java)
- [Collections](https://gitlab.com/mburolla/java-assignments-2/-/blob/dev/src/main/java/com/xpanxion/java/assignments/instructor/Worker0.java)

# Zoom
https://xpanxion.zoom.us/j/97667508928
